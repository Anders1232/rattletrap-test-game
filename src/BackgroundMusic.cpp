#include "BackgroundMusic.h"
#include "GameComponentType.h"

BackgroundMusic::BackgroundMusic(std::string musicFile, RattletrapEngine::GameObject &associated) : Component(associated), music(musicFile)
{
	music.Play(0);
}

BackgroundMusic::~BackgroundMusic()
{
	music.Stop();
}

void BackgroundMusic::Update(float dt){}

bool BackgroundMusic::Is(COMPONENT_TYPE componentType) const
{
	return GameComponentType::BACKGROUND_MUSIC == componentType;
}
