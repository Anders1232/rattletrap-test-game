#include "Face.h"
#include "Sound.h"
#include "GameComponentType.h"

Face::Face(RattletrapEngine::GameObject & associated): RattletrapEngine::Component(associated), hitPoints(30)
{}

void Face::Damage(int damage)
{
	hitPoints -= damage;
	if(0 >= hitPoints)
	{
		hitPoints = 0;
		RattletrapEngine::Sound deathSound("audio/boom.wav");
		deathSound.Play(1);
		associated.RequestDelete();
	}
}

void Face::Update(float dt)
{}

void Face::Render()
{}

bool Face::Is(int componentType)
{
	return componentType == GameComponentType::FACE;
}
