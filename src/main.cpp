#include "Game.h"
#include "TitleState.h"

int main(int argc, char **argv)
{
	RattletrapEngine::Game game("Rattletrap Test Game", 1290, 700);
	game.Push(new TitleState());
	game.Run();
}
