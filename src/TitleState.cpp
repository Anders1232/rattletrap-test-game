#include "TitleState.h"
#include "Sprite.h"
#include "BackgroundMusic.h"
#include "Game.h"
#include "InputManager.h"

TitleState::TitleState(void): RattletrapEngine::State()
{
	RattletrapEngine::GameObject *go= new RattletrapEngine::GameObject();
	RattletrapEngine::Vec2 screenSize= RattletrapEngine::Game::GetInstance().GetWindowDimensions();
	go->box.x=0;
	go->box.y=0;
	go->box.w= screenSize.x;
	go->box.h= screenSize.y;
	go->AddComponent((RattletrapEngine::Component*)new RattletrapEngine::Sprite("img/ocean.jpg", *go));
//	go->AddComponent(new CameraFollower());
	go->AddComponent(new BackgroundMusic("audio/stageState.ogg", *go) );
	RattletrapEngine::State::AddObject(go);
}

void TitleState::Update(float dt)
{
	State::Update(dt);
	if(RattletrapEngine::InputManager::GetInstance().QuitRequested())
	{
		quitRequested=true;
	}
}

void TitleState::Pause(void){}

void TitleState::Resume(void){}

void TitleState::LoadAssets(void) const
{
	
}
