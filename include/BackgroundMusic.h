#ifndef BACKGROUND_MUSIC_H
#define BACKGROUND_MUSIC_H

#include "Music.h"
#include "Component.h"

class BackgroundMusic : public RattletrapEngine::Component
{
	public:
		BackgroundMusic(std::string musicFile, RattletrapEngine::GameObject& associated);
		void Update(float dt);
		bool Is(COMPONENT_TYPE componentType) const;
		~BackgroundMusic();
	private:
		RattletrapEngine::Music music;
};

#endif
