#ifndef FACE_H
#define FACE_H

#include "Component.h"

class Face : RattletrapEngine::Component
{
	public:
		Face(RattletrapEngine::GameObject& associated);
		void Damage(int damage);
		void Update(float dt);
		void Render();
		bool Is(int componentType);
	private:
		int hitPoints;
};

#endif
