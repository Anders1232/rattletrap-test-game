#ifndef TITLE_STATE_H
#define TITLE_STATE_H

#include "State.h"

class TitleState : public RattletrapEngine::State
{
	public:
		TitleState(void);
		void Pause(void);
		void Update(float dt);
		void Resume(void);
		void LoadAssets(void) const;
	private:
	
};

#endif
